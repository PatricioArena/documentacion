﻿using System;

namespace Demo
{
    class Program
    {
        static void Main(string[] args)
        {

#if DEBUG
            Console.WriteLine("Hello World! on DEBUG");
#endif

#if TESTING
            Console.WriteLine("Hello World! on TESTING");
#endif

#if UAT
            Console.WriteLine("Hello World! on UAT");
#endif

#if RELEASE
            Console.WriteLine("Hello World! on RELEASE");
#endif


        }
    }
}
